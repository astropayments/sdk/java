package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class customerscustkeybillingschedulesbillingschedulekeybillingschedulerulesbulkdeleteTest {
	@Test public void customerscustkeybillingschedulesbillingschedulekeybillingschedulerulesbulkdelete() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("status","success");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("custkey","Examplecustkey");
		reqdata.put("billing_schedule_key","Examplebilling_schedule_key");
		reqdata.put("0","ExampleKeys0");
		reqdata.put("1","ExampleKeys1");
		String response = "";
		response = goastro.customers.billing_schedules.billing_schedule_rules.Bulk.delete(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/customers/Examplecustkey/billing_schedules/Examplebilling_schedule_key/billing_schedule_rules/bulk"));
		assertEquals("Method did not match ","DELETE",MockHandler.method.toUpperCase());
	}
}