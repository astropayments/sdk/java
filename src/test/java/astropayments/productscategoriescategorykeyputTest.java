package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class productscategoriescategorykeyputTest {
	@Test public void productscategoriescategorykeyput() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","product_category");
		expected_response.put("key","80v96p6rx2k6sktw");
		expected_response.put("name","Botts Every Flavor Beans");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("category_key","Examplecategory_key");
			reqdata.put("name","Botts Every Flavor Beans");
		String response = "";
		response = goastro.products.Categories.put(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/products/categories/Examplecategory_key"));
		assertEquals("Method did not match ","PUT",MockHandler.method.toUpperCase());
	}
}