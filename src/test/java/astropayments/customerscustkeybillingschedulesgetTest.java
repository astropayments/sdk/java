package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class customerscustkeybillingschedulesgetTest {
	@Test public void customerscustkeybillingschedulesget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","list");
		expected_response.put("limit","0");
		expected_response.put("offset","20");
		List<Map> data = new ArrayList<Map>();
			Map<String, Object> CustomerSchedule = new HashMap<String, Object>();
			CustomerSchedule.put("type","billingschedule");
				List<Map> rules = new ArrayList<Map>();
				Map<String, Object> CustomerRule = new HashMap<String, Object>();
					CustomerRule.put("type","billingschedulerule");
				rules.add(CustomerRule);
				Map<String, Object> CustomerRule1 = new HashMap<String, Object>();
					CustomerRule1.put("type","billingschedulerule");
				rules.add(CustomerRule1);
			CustomerSchedule.put("rules","rules");
		data.add(CustomerSchedule);
			Map<String, Object> CustomerSchedule2 = new HashMap<String, Object>();
			CustomerSchedule2.put("type","billingschedule");
				List<Map> rules3 = new ArrayList<Map>();
				Map<String, Object> CustomerRule4 = new HashMap<String, Object>();
					CustomerRule4.put("type","billingschedulerule");
				rules.add(CustomerRule4);
				Map<String, Object> CustomerRule5 = new HashMap<String, Object>();
					CustomerRule5.put("type","billingschedulerule");
				rules.add(CustomerRule5);
			CustomerSchedule2.put("rules","rules");
		data.add(CustomerSchedule2);
		expected_response.put("data",data);
		expected_response.put("total","1");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("custkey","Examplecustkey");
		String response = "";
		response = goastro.customers.Billing_schedules.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/customers/Examplecustkey/billing_schedules"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}