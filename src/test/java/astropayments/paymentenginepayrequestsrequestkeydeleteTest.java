package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class paymentenginepayrequestsrequestkeydeleteTest {
	@Test public void paymentenginepayrequestsrequestkeydelete() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("status","success");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("requestkey","Examplerequestkey");
		String response = "";
		response = goastro.paymentengine.Payrequests.delete(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/paymentengine/payrequests/Examplerequestkey"));
		assertEquals("Method did not match ","DELETE",MockHandler.method.toUpperCase());
	}
}