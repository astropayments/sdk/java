package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class productsproductkeyputTest {
	@Test public void productsproductkeyput() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","product");
		expected_response.put("key","80v96p6rx2k6sktw");
		expected_response.put("product_refnum","2");
		expected_response.put("name","Botts Every Flavor Beans");
		expected_response.put("price","2.00");
		expected_response.put("enabled","True");
		expected_response.put("taxable","True");
		expected_response.put("categoryid","1238710");
		expected_response.put("commodity_code","3122");
		expected_response.put("description","Every flavor you can imagine and then some.");
		expected_response.put("list_price","3.00");
		expected_response.put("manufacturer","Jelly Belly");
		expected_response.put("merch_productid","EXA1234");
		expected_response.put("physicalgood","True");
		expected_response.put("ship_weight","1.2 oz");
		expected_response.put("sku","123456789");
		expected_response.put("upc","999999999");
		expected_response.put("weight","1.2 oz");
		expected_response.put("wholesale_price","3.22");
		expected_response.put("created","");
		expected_response.put("modified","");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("product_key","Exampleproduct_key");
			reqdata.put("name","Botts Every Flavor Beans");
			reqdata.put("price","2.00");
			reqdata.put("enabled","True");
			reqdata.put("taxable","True");
			reqdata.put("categoryid","1238710");
			reqdata.put("commodity_code","3122");
			reqdata.put("description","Every flavor you can imagine and then some.");
			reqdata.put("list_price","3.00");
			reqdata.put("manufacturer","Jelly Belly");
			reqdata.put("merch_productid","EXA1234");
			reqdata.put("physicalgood","True");
			reqdata.put("ship_weight","1.2 oz");
			reqdata.put("sku","123456789");
			reqdata.put("upc","999999999");
			reqdata.put("weight","1.2 oz");
			reqdata.put("wholesale_price","3.22");
		String response = "";
		response = goastro.Products.put(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/products/Exampleproduct_key"));
		assertEquals("Method did not match ","PUT",MockHandler.method.toUpperCase());
	}
}