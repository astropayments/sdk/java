package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class customerscustkeygetTest {
	@Test public void customerscustkeyget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","customer");
		expected_response.put("key","nsds9yd1h5kb9y5h");
		expected_response.put("custid","123456");
		expected_response.put("company","Weasleys Wizard Wheezes");
		expected_response.put("first_name","George");
		expected_response.put("last_name","Weasley");
		expected_response.put("description","Mischief Managed.");
			List<Map> payment_methods = new ArrayList<Map>();
			List<Map> payment_methods1 = new ArrayList<Map>();
			Map<String, Object> CustomerPaymentMethod = new HashMap<String, Object>();
				CustomerPaymentMethod.put("key","x8KccrxeydHJ4MmT");
				CustomerPaymentMethod.put("type","customerpaymentmethod");
				CustomerPaymentMethod.put("method_name","Example method");
				CustomerPaymentMethod.put("cardholder","Testor Jones");
				CustomerPaymentMethod.put("expiration","1221");
				CustomerPaymentMethod.put("ccnum4last","xxxxxxxxxxxxxx7779");
				CustomerPaymentMethod.put("card_type","Visa");
			payment_methods1.add(CustomerPaymentMethod);
			Map<String, Object> CustomerPaymentMethod2 = new HashMap<String, Object>();
				CustomerPaymentMethod2.put("key","x8KccrxeydHJ4MmT");
				CustomerPaymentMethod2.put("type","customerpaymentmethod");
				CustomerPaymentMethod2.put("method_name","Example method");
				CustomerPaymentMethod2.put("cardholder","Testor Jones");
				CustomerPaymentMethod2.put("expiration","1221");
				CustomerPaymentMethod2.put("ccnum4last","xxxxxxxxxxxxxx7779");
				CustomerPaymentMethod2.put("card_type","Visa");
			payment_methods1.add(CustomerPaymentMethod2);
		expected_response.put("payment_methods","payment_methods");
			List<Map> billing_schedules = new ArrayList<Map>();
			List<Map> billing_schedules3 = new ArrayList<Map>();
			Map<String, Object> CustomerSchedule = new HashMap<String, Object>();
				CustomerSchedule.put("type","billingschedule");
					List<Map> rules = new ArrayList<Map>();
					Map<String, Object> CustomerRule = new HashMap<String, Object>();
						CustomerRule.put("type","billingschedulerule");
					rules.add(CustomerRule);
					Map<String, Object> CustomerRule4 = new HashMap<String, Object>();
						CustomerRule4.put("type","billingschedulerule");
					rules.add(CustomerRule4);
				CustomerSchedule.put("rules","rules");
			billing_schedules3.add(CustomerSchedule);
			Map<String, Object> CustomerSchedule5 = new HashMap<String, Object>();
				CustomerSchedule5.put("type","billingschedule");
					List<Map> rules6 = new ArrayList<Map>();
					Map<String, Object> CustomerRule7 = new HashMap<String, Object>();
						CustomerRule7.put("type","billingschedulerule");
					rules.add(CustomerRule7);
					Map<String, Object> CustomerRule8 = new HashMap<String, Object>();
						CustomerRule8.put("type","billingschedulerule");
					rules.add(CustomerRule8);
				CustomerSchedule5.put("rules","rules");
			billing_schedules3.add(CustomerSchedule5);
		expected_response.put("billing_schedules","billing_schedules");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("custkey","Examplecustkey");
		String response = "";
		response = goastro.Customers.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/customers/Examplecustkey"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}