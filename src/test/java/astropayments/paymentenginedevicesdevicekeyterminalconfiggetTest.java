package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class paymentenginedevicesdevicekeyterminalconfiggetTest {
	@Test public void paymentenginedevicesdevicekeyterminalconfigget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("enable_emv","true");
		expected_response.put("enable_debit_msr","true");
		expected_response.put("enable_tip_adjust","true");
		expected_response.put("enable_contactless","true");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("devicekey","Exampledevicekey");
		String response = "";
		response = goastro.paymentengine.devices.Terminal_config.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/paymentengine/devices/Exampledevicekey/terminal-config"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}