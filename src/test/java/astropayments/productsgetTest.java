package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class productsgetTest {
	@Test public void productsget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","list");
		expected_response.put("limit","0");
		expected_response.put("offset","20");
		List<Map> data = new ArrayList<Map>();
			Map<String, Object> ProductResponse = new HashMap<String, Object>();
			ProductResponse.put("type","product");
			ProductResponse.put("key","80v96p6rx2k6sktw");
			ProductResponse.put("product_refnum","2");
			ProductResponse.put("name","Botts Every Flavor Beans");
			ProductResponse.put("price","2.00");
			ProductResponse.put("enabled","True");
			ProductResponse.put("taxable","True");
			ProductResponse.put("categoryid","1238710");
			ProductResponse.put("commodity_code","3122");
			ProductResponse.put("description","Every flavor you can imagine and then some.");
			ProductResponse.put("list_price","3.00");
			ProductResponse.put("manufacturer","Jelly Belly");
			ProductResponse.put("merch_productid","EXA1234");
			ProductResponse.put("physicalgood","True");
			ProductResponse.put("ship_weight","1.2 oz");
			ProductResponse.put("sku","123456789");
			ProductResponse.put("upc","999999999");
			ProductResponse.put("weight","1.2 oz");
			ProductResponse.put("wholesale_price","3.22");
			ProductResponse.put("created","");
			ProductResponse.put("modified","");
		data.add(ProductResponse);
			Map<String, Object> ProductResponse1 = new HashMap<String, Object>();
			ProductResponse1.put("type","product");
			ProductResponse1.put("key","80v96p6rx2k6sktw");
			ProductResponse1.put("product_refnum","2");
			ProductResponse1.put("name","Botts Every Flavor Beans");
			ProductResponse1.put("price","2.00");
			ProductResponse1.put("enabled","True");
			ProductResponse1.put("taxable","True");
			ProductResponse1.put("categoryid","1238710");
			ProductResponse1.put("commodity_code","3122");
			ProductResponse1.put("description","Every flavor you can imagine and then some.");
			ProductResponse1.put("list_price","3.00");
			ProductResponse1.put("manufacturer","Jelly Belly");
			ProductResponse1.put("merch_productid","EXA1234");
			ProductResponse1.put("physicalgood","True");
			ProductResponse1.put("ship_weight","1.2 oz");
			ProductResponse1.put("sku","123456789");
			ProductResponse1.put("upc","999999999");
			ProductResponse1.put("weight","1.2 oz");
			ProductResponse1.put("wholesale_price","3.22");
			ProductResponse1.put("created","");
			ProductResponse1.put("modified","");
		data.add(ProductResponse1);
		expected_response.put("data",data);
		expected_response.put("total","42");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("limit","Examplelimit");
		reqdata.put("offset","Exampleoffset");
		String response = "";
		response = goastro.Products.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/products"));
		assertTrue("Path missing query parameter limit",MockHandler.path.contains("Examplelimit"));
		assertTrue("Path missing query parameter offset",MockHandler.path.contains("Exampleoffset"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}