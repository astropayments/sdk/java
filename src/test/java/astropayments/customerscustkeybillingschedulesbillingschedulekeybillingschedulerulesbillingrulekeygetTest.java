package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class customerscustkeybillingschedulesbillingschedulekeybillingschedulerulesbillingrulekeygetTest {
	@Test public void customerscustkeybillingschedulesbillingschedulekeybillingschedulerulesbillingrulekeyget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","billingschedulerule");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("custkey","Examplecustkey");
		reqdata.put("billing_schedule_key","Examplebilling_schedule_key");
		reqdata.put("billing_rule_key","Examplebilling_rule_key");
		String response = "";
		response = goastro.customers.billing_schedules.Billing_schedule_rules.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/customers/Examplecustkey/billing_schedules/Examplebilling_schedule_key/billing_schedule_rules/Examplebilling_rule_key"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}