package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class customerscustkeybillingschedulespostTest {
	@Test public void customerscustkeybillingschedulespost() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		List<Map> expected_response = new ArrayList<Map>();
		Map<String, Object> CustomerSchedule = new HashMap<String, Object>();
			CustomerSchedule.put("type","billingschedule");
				List<Map> rules = new ArrayList<Map>();
				Map<String, Object> CustomerRule = new HashMap<String, Object>();
					CustomerRule.put("type","billingschedulerule");
				rules.add(CustomerRule);
				Map<String, Object> CustomerRule1 = new HashMap<String, Object>();
					CustomerRule1.put("type","billingschedulerule");
				rules.add(CustomerRule1);
			CustomerSchedule.put("rules","rules");
		expected_response.add(CustomerSchedule);
		Map<String, Object> CustomerSchedule2 = new HashMap<String, Object>();
			CustomerSchedule2.put("type","billingschedule");
				List<Map> rules3 = new ArrayList<Map>();
				Map<String, Object> CustomerRule4 = new HashMap<String, Object>();
					CustomerRule4.put("type","billingschedulerule");
				rules.add(CustomerRule4);
				Map<String, Object> CustomerRule5 = new HashMap<String, Object>();
					CustomerRule5.put("type","billingschedulerule");
				rules.add(CustomerRule5);
			CustomerSchedule2.put("rules","rules");
		expected_response.add(CustomerSchedule2);
		String encoded_response = API.json_encode_list(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("custkey","Examplecustkey");
			Map<String, Object> request_item0 = new HashMap<String, Object>();
			request_item0.put("type","billingschedule");
				List<Map> rules6 = new ArrayList<Map>();
				List<Map> rules7 = new ArrayList<Map>();
				Map<String, Object> CustomerRule8 = new HashMap<String, Object>();
					CustomerRule8.put("type","billingschedulerule");
				rules7.add(CustomerRule8);
				Map<String, Object> CustomerRule9 = new HashMap<String, Object>();
					CustomerRule9.put("type","billingschedulerule");
				rules7.add(CustomerRule9);
			request_item0.put("rules","rules");
		reqdata.put("0",request_item0);
			Map<String, Object> request_item1 = new HashMap<String, Object>();
			request_item1.put("type","billingschedule");
				List<Map> rules10 = new ArrayList<Map>();
				List<Map> rules11 = new ArrayList<Map>();
				Map<String, Object> CustomerRule12 = new HashMap<String, Object>();
					CustomerRule12.put("type","billingschedulerule");
				rules11.add(CustomerRule12);
				Map<String, Object> CustomerRule13 = new HashMap<String, Object>();
					CustomerRule13.put("type","billingschedulerule");
				rules11.add(CustomerRule13);
			request_item1.put("rules","rules");
		reqdata.put("1",request_item1);
		String response = "";
		response = goastro.customers.Billing_schedules.post(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/customers/Examplecustkey/billing_schedules"));
		assertEquals("Method did not match ","POST",MockHandler.method.toUpperCase());
	}
}