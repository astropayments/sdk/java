package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class transactionstrankeysendpostTest {
	@Test public void transactionstrankeysendpost() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("status","success");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("trankey","Exampletrankey");
			reqdata.put("toemail","wulfric@hogwarts.com");
			reqdata.put("template","vterm_customer");
		String response = "";
		response = goastro.transactions.Send.post(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/transactions/Exampletrankey/send"));
		assertEquals("Method did not match ","POST",MockHandler.method.toUpperCase());
	}
}