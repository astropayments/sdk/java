package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class customerscustkeybillingschedulesbillingschedulekeybillingschedulerulesgetTest {
	@Test public void customerscustkeybillingschedulesbillingschedulekeybillingschedulerulesget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","list");
		expected_response.put("limit","0");
		expected_response.put("offset","20");
		List<Map> data = new ArrayList<Map>();
			Map<String, Object> CustomerRule = new HashMap<String, Object>();
			CustomerRule.put("type","billingschedulerule");
		data.add(CustomerRule);
			Map<String, Object> CustomerRule1 = new HashMap<String, Object>();
			CustomerRule1.put("type","billingschedulerule");
		data.add(CustomerRule1);
		expected_response.put("data",data);
		expected_response.put("total","1");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("custkey","Examplecustkey");
		reqdata.put("billing_schedule_key","Examplebilling_schedule_key");
		String response = "";
		response = goastro.customers.billing_schedules.Billing_schedule_rules.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/customers/Examplecustkey/billing_schedules/Examplebilling_schedule_key/billing_schedule_rules"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}