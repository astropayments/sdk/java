package goastro;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.google.gson.*;
import goastro.*;
import java.util.*;


public class paymentenginedevicesgetTest {
	@Test public void paymentenginedevicesget() {
		API.setAuthentication("Example_API_Key","Example_Pin");
		API.local_test=true;

		//Building Response
		Map<String, Object> expected_response = new HashMap<String, Object>();
		expected_response.put("type","list");
		expected_response.put("limit","0");
		expected_response.put("offset","20");
		List<Map> data = new ArrayList<Map>();
			Map<String, Object> DeviceResponse = new HashMap<String, Object>();
			DeviceResponse.put("type","device");
			DeviceResponse.put("key","sa_WKwzyQawBG0RMy0XpDGFXb6pXA32r");
			DeviceResponse.put("apikeyid","ntC8nP31Moh0wtvYT");
			DeviceResponse.put("terminal_type","standalone");
			DeviceResponse.put("status","ntC8nP31Moh0wtvYT");
			DeviceResponse.put("name","ntC8nP31Moh0wtvYT");
				Map<String, Object> settings = new HashMap<String, Object>();
				settings.put("timeout","30");
				settings.put("enable_standalone","false");
				settings.put("share_device","false");
				settings.put("notify_update","true");
				settings.put("notify_update_next","true");
				settings.put("sleep_battery_device","5");
				settings.put("sleep_battery_display","3");
				settings.put("sleep_powered_device","10");
				settings.put("sleep_powered_display","8");
			DeviceResponse.put("settings","settings");
				Map<String, Object> terminal_info = new HashMap<String, Object>();
				terminal_info.put("make","Castle");
				terminal_info.put("model","Vega3000");
				terminal_info.put("revision","18043001-0055.00");
				terminal_info.put("serial","011118170300198");
				terminal_info.put("key_pin","FFFF5B04");
				terminal_info.put("key_pan","FF908A");
			DeviceResponse.put("terminal_info","terminal_info");
				Map<String, Object> terminal_config = new HashMap<String, Object>();
				terminal_config.put("enable_emv","true");
				terminal_config.put("enable_debit_msr","true");
				terminal_config.put("enable_tip_adjust","true");
				terminal_config.put("enable_contactless","true");
			DeviceResponse.put("terminal_config","terminal_config");
			DeviceResponse.put("pairing_code","690520");
			DeviceResponse.put("expiration","2019-10-29 11:59:28");
		data.add(DeviceResponse);
			Map<String, Object> DeviceResponse1 = new HashMap<String, Object>();
			DeviceResponse1.put("type","device");
			DeviceResponse1.put("key","sa_WKwzyQawBG0RMy0XpDGFXb6pXA32r");
			DeviceResponse1.put("apikeyid","ntC8nP31Moh0wtvYT");
			DeviceResponse1.put("terminal_type","standalone");
			DeviceResponse1.put("status","ntC8nP31Moh0wtvYT");
			DeviceResponse1.put("name","ntC8nP31Moh0wtvYT");
				Map<String, Object> settings2 = new HashMap<String, Object>();
				settings.put("timeout","30");
				settings.put("enable_standalone","false");
				settings.put("share_device","false");
				settings.put("notify_update","true");
				settings.put("notify_update_next","true");
				settings.put("sleep_battery_device","5");
				settings.put("sleep_battery_display","3");
				settings.put("sleep_powered_device","10");
				settings.put("sleep_powered_display","8");
			DeviceResponse1.put("settings","settings");
				Map<String, Object> terminal_info3 = new HashMap<String, Object>();
				terminal_info.put("make","Castle");
				terminal_info.put("model","Vega3000");
				terminal_info.put("revision","18043001-0055.00");
				terminal_info.put("serial","011118170300198");
				terminal_info.put("key_pin","FFFF5B04");
				terminal_info.put("key_pan","FF908A");
			DeviceResponse1.put("terminal_info","terminal_info");
				Map<String, Object> terminal_config4 = new HashMap<String, Object>();
				terminal_config.put("enable_emv","true");
				terminal_config.put("enable_debit_msr","true");
				terminal_config.put("enable_tip_adjust","true");
				terminal_config.put("enable_contactless","true");
			DeviceResponse1.put("terminal_config","terminal_config");
			DeviceResponse1.put("pairing_code","690520");
			DeviceResponse1.put("expiration","2019-10-29 11:59:28");
		data.add(DeviceResponse1);
		expected_response.put("data",data);
		expected_response.put("total","42");
		String encoded_response = API.json_encode(expected_response);
		MockHandler.response = encoded_response;

		//Building Request
		Map<String, Object> reqdata = new HashMap<String, Object>();
		reqdata.put("limit","Examplelimit");
		reqdata.put("offset","Exampleoffset");
		String response = "";
		response = goastro.paymentengine.Devices.get(reqdata);

		assertEquals("Response did not match ",encoded_response,response);
		assertEquals("Request did not match ",API.json_encode(reqdata),MockHandler.request);
		assertTrue("Path not formatted correctly "+MockHandler.path,MockHandler.path.contains("https://api.goastro.com/api/v2/paymentengine/devices"));
		assertTrue("Path missing query parameter limit",MockHandler.path.contains("Examplelimit"));
		assertTrue("Path missing query parameter offset",MockHandler.path.contains("Exampleoffset"));
		assertEquals("Method did not match ","GET",MockHandler.method.toUpperCase());
	}
}