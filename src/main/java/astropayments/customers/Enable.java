package goastro.customers;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Enable{

	public static String post(Map data){
		String path="";


		path = "/customers/enable";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
