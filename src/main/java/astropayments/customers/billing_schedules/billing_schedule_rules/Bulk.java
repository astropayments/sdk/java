package goastro.customers.billing_schedules.billing_schedule_rules;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Bulk{

	public static String delete(Map data){
		String path="";

	
if(!data.containsKey("custkey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing custkey");
			return API.json_encode(res);
		}

	
if(!data.containsKey("billing_schedule_key")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing billing_schedule_key");
			return API.json_encode(res);
		}


		path = "/customers/" + data.get("custkey") + "/billing_schedules/" + data.get("billing_schedule_key") + "/billing_schedule_rules/bulk";

		try{
			return API.runCall("delete",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
