package goastro.customers.billing_schedules;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Billing_schedule_rules{

	public static String get(Map data){
		String path="";

	
if(!data.containsKey("custkey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing custkey");
			return API.json_encode(res);
		}

	
if(!data.containsKey("billing_schedule_key")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing billing_schedule_key");
			return API.json_encode(res);
		}


		path = "/customers/" + data.get("custkey") + "/billing_schedules/" + data.get("billing_schedule_key") + "/billing_schedule_rules";

		if(data.containsKey("billing_rule_key")){
			path = "/customers/" + data.get("custkey") + "/billing_schedules/" + data.get("billing_schedule_key") + "/billing_schedule_rules/" + data.get("billing_rule_key");
			data.remove("billing_rule_key");
		}

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String delete(Map data){
		String path="";

	
if(!data.containsKey("custkey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing custkey");
			return API.json_encode(res);
		}

	
if(!data.containsKey("billing_schedule_key")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing billing_schedule_key");
			return API.json_encode(res);
		}


		if(data.containsKey("billing_rule_key")){
			path = "/customers/" + data.get("custkey") + "/billing_schedules/" + data.get("billing_schedule_key") + "/billing_schedule_rules/" + data.get("billing_rule_key");
			data.remove("billing_rule_key");
		}

		try{
			return API.runCall("delete",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String put(Map data){
		String path="";

	
if(!data.containsKey("custkey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing custkey");
			return API.json_encode(res);
		}

	
if(!data.containsKey("billing_schedule_key")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing billing_schedule_key");
			return API.json_encode(res);
		}


		if(data.containsKey("billing_rule_key")){
			path = "/customers/" + data.get("custkey") + "/billing_schedules/" + data.get("billing_schedule_key") + "/billing_schedule_rules/" + data.get("billing_rule_key");
			data.remove("billing_rule_key");
		}

		try{
			return API.runCall("put",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
