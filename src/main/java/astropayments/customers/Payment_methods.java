package goastro.customers;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Payment_methods{

	public static String get(Map data){
		String path="";

	
if(!data.containsKey("custkey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing custkey");
			return API.json_encode(res);
		}


		path = "/customers/" + data.get("custkey") + "/payment_methods";

		if(data.containsKey("paymethod_key")){
			path = "/customers/" + data.get("custkey") + "/payment_methods/" + data.get("paymethod_key");
			data.remove("paymethod_key");
		}

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String post(Map data){
		String path="";

	
if(!data.containsKey("custkey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing custkey");
			return API.json_encode(res);
		}


		path = "/customers/" + data.get("custkey") + "/payment_methods";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String delete(Map data){
		String path="";

	
if(!data.containsKey("custkey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing custkey");
			return API.json_encode(res);
		}


		if(data.containsKey("paymethod_key")){
			path = "/customers/" + data.get("custkey") + "/payment_methods/" + data.get("paymethod_key");
			data.remove("paymethod_key");
		}

		try{
			return API.runCall("delete",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String put(Map data){
		String path="";

	
if(!data.containsKey("custkey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing custkey");
			return API.json_encode(res);
		}


		if(data.containsKey("paymethod_key")){
			path = "/customers/" + data.get("custkey") + "/payment_methods/" + data.get("paymethod_key");
			data.remove("paymethod_key");
		}

		try{
			return API.runCall("put",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
