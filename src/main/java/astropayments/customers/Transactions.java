package goastro.customers;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Transactions{

	public static String get(Map data){
		String path="";

	
if(!data.containsKey("custkey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing custkey");
			return API.json_encode(res);
		}


		path = "/customers/" + data.get("custkey") + "/transactions";

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
