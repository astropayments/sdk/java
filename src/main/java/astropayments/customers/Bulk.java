package goastro.customers;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Bulk{

	public static String delete(Map data){
		String path="";


		path = "/customers/bulk";

		try{
			return API.runCall("delete",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
