package goastro;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Products{

	public static String get(Map data){
		String path="";

		HashMap params = new HashMap<String, String>();
		if(data.containsKey("limit")) params.put("limit",data.get("limit"));
		if(data.containsKey("offset")) params.put("offset",data.get("offset"));

		path = "/products";

		if(data.containsKey("product_key")){
			path = "/products/" + data.get("product_key");
			data.remove("product_key");
		}

		try{
			return API.runCall("get",path,data,params);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String post(Map data){
		String path="";


		path = "/products";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String delete(Map data){
		String path="";


		if(data.containsKey("product_key")){
			path = "/products/" + data.get("product_key");
			data.remove("product_key");
		}

		try{
			return API.runCall("delete",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}

	public static String put(Map data){
		String path="";


		if(data.containsKey("product_key")){
			path = "/products/" + data.get("product_key");
			data.remove("product_key");
		}

		try{
			return API.runCall("put",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
