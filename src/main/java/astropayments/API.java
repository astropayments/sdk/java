package goastro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;


import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Base64;
import java.util.Map;
import java.util.HashMap;
import com.google.gson.*;

import javax.net.ssl.*;
import java.util.*;

public class API {
    static String password = "";
   
   static String subdomain = "api";
   
   static String endpoint_key = "v2";

   static boolean local_test=false;

    public static void setAuthentication(String apiKey, String apiPin){
        String seed = "abcdefghijklmnop";
        String prehash = apiKey + seed + apiPin;
        String apihash;
        try {
            apihash = "s2/" + seed + "/" + hashSHA256(prehash);
        } catch(Exception e){
            e.printStackTrace();
            return;
        }
        String encodeBytes = Base64.getEncoder().encodeToString((apiKey + ":" +apihash).getBytes());
        String header = ("Basic " + encodeBytes);
        password = header;
    }

    public static void setSubdomain(String new_subdomain){
      subdomain = new_subdomain;
    }

    public static void setEndpointKey(String new_endpoint_key){
      endpoint_key = new_endpoint_key;
    }

    public static String runCall(String type, String path, Map data, Map params){
        //params go into path
        Boolean first = true;
        for(Object param : params.keySet()){
            String param_value = data.get(param).toString();

            if(!param_value.isEmpty()){
                if(first){
                    path += "?" + param + "=" + param_value;
                    first = false;
                }
                else{
                    path += "&" + param + "=" + param_value;
                }
            }
        }
        String service_url = "https://" + subdomain + ".goastro.com/api/" + endpoint_key + path;

        if(local_test){
            return MockHandler.mockCall(type,service_url,json_encode(data));
        }

        try {
            return sendJson(type,service_url,json_encode(data));
        } catch (Exception e){
            e.printStackTrace();
            return "Error";
        }
    }

    public static String runCall(String type, String path, Map data){
        return runCall(type,path,data, new HashMap<String,String>());
    }

    private static String hashSHA256(String originalString) throws NoSuchAlgorithmException {

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(
                originalString.getBytes(StandardCharsets.UTF_8));

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < encodedhash.length; i++) {
            String hex = Integer.toHexString(0xff & encodedhash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static String json_encode(Map data){
        Gson gsonObj = new Gson();
        return gsonObj.toJson(data);
    }

    public static String json_encode_list(List<Map> data){
        Gson gsonObj = new Gson();
        return gsonObj.toJson(data);
    }

    public static String json_encode_string(String data){
        Gson gsonObj = new Gson();
        return gsonObj.toJson(data);
    }
    public static void trustAllHosts()
    {
        try
        {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509ExtendedTrustManager()
                    {
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers()
                        {
                            return null;
                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] xcs, String string, Socket socket) throws CertificateException
                        {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] xcs, String string, Socket socket) throws CertificateException
                        {

                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException
                        {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException
                        {

                        }

                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new  HostnameVerifier()
            {
                @Override
                public boolean verify(String hostname, SSLSession session)
                {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        }
        catch (Exception e)
        {
        }
    }

    public static String sendJson(String type, String path, String json) throws MalformedURLException, IOException {
        URL myurl = new URL(path);
        HttpURLConnection con = (HttpURLConnection)myurl.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);

        con.setRequestProperty("Content-Type", "application/json;");
        con.setRequestProperty("Accept", "application/json,text/plain");
        con.setRequestMethod(type.toUpperCase());
        con.setRequestProperty("Authorization", password);

        if(!type.toUpperCase().equals("GET") ){
            OutputStream os = con.getOutputStream();
            os.write(json.toString().getBytes("UTF-8"));
            os.close();
        }

        StringBuilder sb = new StringBuilder();
        int HttpResult =con.getResponseCode();
        if(HttpResult ==HttpURLConnection.HTTP_OK){
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(),"utf-8"));

            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            br.close();

            return sb.toString();

        }else{
            // Return error as a json string
            Map<String, Object> res = new HashMap<String, Object>();
            res.put("ErrorCode",con.getResponseCode());
            res.put("ErrorMessage",con.getResponseMessage());
            return json_encode(res);
        }

    }

}
