package goastro.transactions;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Receipts{

	public static String get(Map data){
		String path="";

	
if(!data.containsKey("trankey")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing trankey");
			return API.json_encode(res);
		}


		if(data.containsKey("receipt_key")){
			path = "/transactions/" + data.get("trankey") + "/receipts/" + data.get("receipt_key");
			data.remove("receipt_key");
		}

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
