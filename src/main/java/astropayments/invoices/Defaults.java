package goastro.invoices;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Defaults{

	public static String get(Map data){
		String path="";


		path = "/invoices/defaults";

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
