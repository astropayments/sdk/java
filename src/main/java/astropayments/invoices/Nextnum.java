package goastro.invoices;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Nextnum{

	public static String get(Map data){
		String path="";


		path = "/invoices/nextnum";

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
