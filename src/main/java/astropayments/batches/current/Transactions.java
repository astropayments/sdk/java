package goastro.batches.current;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Transactions{

	public static String get(Map data){
		String path="";

		HashMap params = new HashMap<String, String>();
		if(data.containsKey("limit")) params.put("limit",data.get("limit"));
		if(data.containsKey("offset")) params.put("offset",data.get("offset"));
		if(data.containsKey("return_bin")) params.put("return_bin",data.get("return_bin"));

		path = "/batches/current/transactions";

		try{
			return API.runCall("get",path,data,params);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
