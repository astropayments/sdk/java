package goastro.batches.current;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Close{

	public static String post(Map data){
		String path="";


		path = "/batches/current/close";

		try{
			return API.runCall("post",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
