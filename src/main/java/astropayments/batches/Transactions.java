package goastro.batches;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Transactions{

	public static String get(Map data){
		String path="";

	
if(!data.containsKey("batch_key")){
			HashMap res = new HashMap<String, String>();
			res.put("ErrorMessage","Missing batch_key");
			return API.json_encode(res);
		}

		HashMap params = new HashMap<String, String>();
		if(data.containsKey("limit")) params.put("limit",data.get("limit"));
		if(data.containsKey("offset")) params.put("offset",data.get("offset"));
		if(data.containsKey("return_bin")) params.put("return_bin",data.get("return_bin"));

		path = "/batches/" + data.get("batch_key") + "/transactions";

		try{
			return API.runCall("get",path,data,params);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
