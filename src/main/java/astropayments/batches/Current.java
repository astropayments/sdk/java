package goastro.batches;

import goastro.API;

import java.util.Map;
import java.util.HashMap;


public class Current{

	public static String get(Map data){
		String path="";


		path = "/batches/current";

		try{
			return API.runCall("get",path,data);
		}
		catch(Exception e){
			e.printStackTrace();
			return "Error";
		}
	}
}
